package com.qidi.socket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NettyApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(NettyApplication.class);
		app.setWebEnvironment(false);
		app.run(args);
	}
}
