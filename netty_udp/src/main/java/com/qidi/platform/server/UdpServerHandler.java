package com.qidi.platform.server;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import io.netty.util.CharsetUtil;

import java.util.concurrent.ThreadLocalRandom;

/**
 * 
 * 包      名 ：  com.qidi.platform.client
 * 文 件 名 : UdpServerHandler
 * 描       述: TODO
 * 作        者： 超哥
 * 创建时间： 2017年2月22日 下午4:38:17
 * 版         本：1.0
 */
public class UdpServerHandler extends SimpleChannelInboundHandler<DatagramPacket> {
    
	private static final  String[] DICTIONARY={
            "看我的，hello world",
            "C++",
            "C#",
            "Java",
            "python"
    };

    private String nextQuoto(){
        // 线程安全的随机类：ThreadLocalRandom
        int quoteId = ThreadLocalRandom.current().nextInt(DICTIONARY.length);
        return DICTIONARY[quoteId];
    }

  

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx,Throwable cause)throws Exception{
        ctx.close();
        cause.printStackTrace();
    }


	/* (non-Javadoc)
	 * @see io.netty.channel.SimpleChannelInboundHandler#channelRead0(io.netty.channel.ChannelHandlerContext, java.lang.Object)
	 * asfasdf
	 */
	@Override
	protected void channelRead0(ChannelHandlerContext channelHandlerContext, DatagramPacket datagramPacket)
			throws Exception {
	     // 因为Netty对UDP进行了封装，所以接收到的是DatagramPacket对象。
        String req = datagramPacket.content().toString(CharsetUtil.UTF_8);
        System.out.println(req);

        if("111".equals(req)){
            channelHandlerContext.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(
                    "结果："+nextQuoto(),CharsetUtil.UTF_8),datagramPacket.sender()));
        }
		
	}
}