package com.qidi.zipkin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import springfox.documentation.swagger2.annotations.EnableSwagger2;
import zipkin.server.EnableZipkinServer;

/**
 * 
 * 包      名 ：  com.qidi.zipkin
 * 文 件 名 : App
 * 描       述: @url = "https://yq.aliyun.com/articles/60165"
 * 作        者： 超哥
 * 创建时间： 2017年2月28日 下午3:32:33
 * 版         本：1.0
 */
@SpringBootApplication
@EnableZipkinServer
@RestController
public class App 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(App.class, args);
    }
    
    
    
   
    @RequestMapping("/test")
    @ResponseBody
    public String test(){
    	
    	return  "11";
    	
    }
}
