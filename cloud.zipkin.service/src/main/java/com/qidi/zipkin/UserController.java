package com.qidi.zipkin;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiOperation;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/users")     // 通过这里配置使下面的映射都在/users下，可去除
public class UserController {
    
	/**
	 * 我们通过@ApiOperation注解来给API增加说明、通过@ApiImplicitParams、@ApiImplicitParam注解来给参数增加说明。
	 * 方法名：getUserList
	 * 功能描述: 描述该
	 *
	 * @return
	 *
	 * 作者： 超哥
	 * 创建时间： 2017年2月28日 下午4:29:44
	 *
	 */

    @ApiOperation(value="获取用户列表", notes="")
    @RequestMapping(value={"/user"}, method=RequestMethod.GET)
    public List<User> getUserList() {
        List<User> r = new ArrayList<User>();
        return r;
    }

 
}