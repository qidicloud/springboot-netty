package cloud.eureka.server;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPipeline;
import redis.clients.jedis.Transaction;

/**
 * @author 超哥
 * @Time：2017年1月3日 下午7:36:17
 * @version 1.0
 * 
 * 事务和管道都是异步模式。在事务和管道中不能同步查询结果。比如下面两个调用，都是不允许的：
 */
public class RedisTest2 {
	
	public static void main(String[] args) {
		
		 
		Jedis  jedis = new Jedis("192.168.0.97", 6379);
		jedis.auth("1063420481");//权限认证
		
		//jedis.set("name","xinxin");//向key-->name中放入了value-->xinxin  
		 jedis.set("name1", "zhouchao", "NX", "EX", 10);  //EX  秒
		
		 
		 
		System.out.println(jedis.get("name1"));//执行结果：xinxin  
		
	}
	
	/**
	 * 有时，我们需要采用异步方式，一次发送多个指令，不同步等待其返回结果。这样可以取得非常好的执行效率。这就是管道，调用方法如下：
	 */
	@Test
	public void test3Pipelined() {
		Jedis  jedis = new Jedis("123.207.242.151", 6379);
	    Pipeline pipeline = jedis.pipelined();
	    long start = System.currentTimeMillis();
	    for (int i = 0; i < 10000; i++) {
	        pipeline.set("p" + i, "p" + i);
	    }
	    List<Object> results = pipeline.syncAndReturnAll();
	    long end = System.currentTimeMillis();
	    System.out.println("Pipelined SET: " + ((end - start)/1000.0) + " seconds");
	    jedis.disconnect();
	}
	
	
	/**
	 * 
	 * 方法名：test2Trans
	 * 功能描述: redis 事物
	 *
	 *
	 * 作者： 超哥
	 * 创建时间： 2017年1月3日 下午9:56:56
	 *
	 */
	@Test
	public void test2Trans() {
		Jedis  jedis = new Jedis("123.207.242.151", 6379);
	   
	    Transaction tx = jedis.multi();
	    for (int i = 0; i < 1000000; i++) {
	       // tx.set("t" + i, "t" + i);
	        tx.set("t"+i,  "t" + i, "NX", "EX", 10);  //EX  秒
	    }
	    long start = System.currentTimeMillis();
	    List<Object> results = tx.exec();
	    long end = System.currentTimeMillis();
	    System.out.println("Transaction SET: " + ((end - start)/1000.0) + " seconds");
	    jedis.disconnect();
	}
	
	//分布式直连同步调用
	/**
	 * 
	 * 方法名：test5shardNormal
	 * 功能描述: 这个是分布式直接连接，并且是同步调用，每步执行都返回执行结果。类似地，还有异步管道调用。
	 *
	 *
	 * 作者： 超哥
	 * 创建时间： 2017年1月3日 下午10:03:34
	 *
	 */
	@Test
	public void test5shardNormal() {
		
	    List<JedisShardInfo> shards = Arrays.asList(
	            new JedisShardInfo("123.207.242.151",6379),
	            new JedisShardInfo("123.207.242.151",6379));

	    ShardedJedis sharding = new ShardedJedis(shards);
	    ShardedJedisPipeline pipeline =  sharding.pipelined(); //管道
	    long start = System.currentTimeMillis();
	    for (int i = 0; i < 100000; i++) {
	          pipeline.set("sn" + i, "n" + i);
	    }
	    
	     List<Object> results = pipeline.syncAndReturnAll(); //异步返回
	    long end = System.currentTimeMillis();
	    System.out.println("Simple@Sharing SET: " + ((end - start)/1000.0) + " seconds");

	    sharding.disconnect();
	}

}
