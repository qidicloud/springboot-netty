package com.qidi.platform.server;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;

/**
 * 
 * 包      名 ：  com.qidi.platform.server
 * 文 件 名 : UdpServer
 * 描       述: TODO
 * 作        者： 超哥
 * 创建时间： 2017年2月22日 下午4:45:36
 * 版         本：1.0
 */
public class UdpServer {

    // 相比于TCP而言，UDP不存在客户端和服务端的实际链接，因此
    // 不需要为连接(ChannelPipeline)设置handler
    public void run(int port)throws Exception{
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(group).channel(NioDatagramChannel.class)
                    .option(ChannelOption.SO_BROADCAST,true)
                    .handler(new UdpServerHandler());

            b.bind(port).sync().channel().closeFuture().await();
        }
        finally {
            group.shutdownGracefully();
        }
    }

    public static void main(String[] args) throws Exception{
        int port = 8011;
        if(args.length>0){
            try{
                port = Integer.parseInt(args[0]);
            }
            catch (NumberFormatException e){
                e.printStackTrace();
            }
        }
        new UdpServer().run(port);
    }
}