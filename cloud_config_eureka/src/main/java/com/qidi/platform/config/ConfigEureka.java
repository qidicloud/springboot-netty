/**  
 * 文 件 名 : App
 * 描       述: TODO
 * 作        者： 超哥
 * 创建时间： 2017年3月1日 下午1:04:59
 * 版         本：1.0
 */
package com.qidi.platform.config;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;


/**  
 * 包      名 ：  com.qidi.platform.config
 * 文 件 名 : App
 * 描       述: 分布式统一配置 加 注册中心
 * 作        者： 超哥
 * 创建时间： 2017年3月1日 下午1:04:59
 * 版         本：1.0
 */
@SpringBootApplication
@EnableConfigServer       //配置服务
@EnableDiscoveryClient    // eureka 客户端
public class ConfigEureka {
	
	public static void main(String[] args) {
		new SpringApplicationBuilder(ConfigEureka.class).web(true).run(args);
		
	}

}
