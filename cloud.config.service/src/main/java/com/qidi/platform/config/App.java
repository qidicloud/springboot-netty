/**  
 * 文 件 名 : App
 * 描       述: TODO
 * 作        者： 超哥
 * 创建时间： 2017年3月1日 上午10:33:54
 * 版         本：1.0
 */
package com.qidi.platform.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**  
 * 包      名 ：  com.qidi.platform.config
 * 文 件 名 : App
 * 描       述: 测试连接 http://localhost:8888/cloud-config/dev
 * 作        者： 超哥
 * 创建时间： 2017年3月1日 上午10:33:54
 * 版         本：1.0
 */
@SpringBootApplication
@EnableConfigServer
public class App {
	
	public static void main(String[] args) {
		
		
		 SpringApplication.run(App.class, args);
	}

}
